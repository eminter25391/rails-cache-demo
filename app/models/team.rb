class Team < ActiveRecord::Base
  belongs_to :company, touch: true
  has_many :members
end
