module CompaniesHelper

  def cache_key_for_companies
    count = Company.count
    updated_at = Company.maximum(:updated_at)

    return "view/companies/#{count}-#{updated_at}"

  end

  def cache_key_for_teams
    count = Team.count
    updated_at = Team.maximum(:updated_at)

    return "view/teams/#{count}-#{updated_at}"

  end
end
