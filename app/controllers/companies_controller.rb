class CompaniesController < ApplicationController

  def index
    @companies = Company.preload(teams: :members)
    @teams = Team.preload(:members)
  end
end
