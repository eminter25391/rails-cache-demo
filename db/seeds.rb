# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

10.times do
  Company.create(name: FFaker::Company.name, email: FFaker::Internet.email)
  10.times do |i|
    Team.create(name: FFaker::Sport.name + ' ' + FFaker::Name.last_name, company_id: Random.rand(1..10))
    20.times do |i|
      Member.create(name: FFaker::Name.name, email: FFaker::Internet.email, age: Random.rand(20..50), team_id: Random.rand(1..100))
    end
  end
end


